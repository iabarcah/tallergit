#include <stdio.h>
#include <stdlib.h>
#include "estudiantes.h"
#include <math.h>
#include <string.h>
//Funcion paradeterminar la  desviacion estandar de los promedios totales del curso
float desvStd(estudiante curso[]){
	//printf("entro d_S\n");
	int i=0,sumator=0,deno=0;//se determinan unas variables enteras 
	float pr_1=0, num,num_2,fra,des=0; //y unas variables con decimales
	for (i=0;i<24;i++){//se genera un for para poder sumar los promedios y obtener cuantos hay
		if (curso[i].prom!=0){
			pr_1=pr_1+curso[i].prom;
			deno++;
		}
	}
	pr_1=pr_1/deno;//se divide la suma del promedio con la cantidad, sacando el promedio general de 
	for(i=0;i<24;i++){//se genera un for para poder determinar lo que esta en el numerador de la  D.S
		num=curso[i].prom-pr_1;
		num_2=pow(num,2);
		sumator=sumator+num_2;
	}
	fra=sumator/deno;//se divide lo obtenido en el numerador por la cantidad de promedios obtenidos
	des=sqrt(fra);//se obtiene la raiz cuadrada de lo anterior 
	return des;//se obtiene la desviacion estandar
}
//funcionn para ver el promedio menor del curso
float menor(estudiante curso[]){
	//printf("Entro mennor\n");
	float  pr_me=7.0;//se  genera el promedio alto para ya tomar desde el primer valor  y asi compararlo con los otros promedios
	int i=0;
	for (i=0;i<24;i++){//reptitiva para comparar promedios y determinar el menor
		//if (i=0)pr_me=curso[i].prom;
		if (curso[i].prom<pr_me)pr_me=curso[i].prom;//si el promedio actual es mas pequeño que el de la variable, la variable tomara un nuevo valor
	}

	return pr_me;
}
//funcion para obtener el promedio mayor del curso
float mayor(estudiante curso[]){
	//printf("entro mayor\n");	
	float  pr_ma=0.0;//se  define unn promedio menor para poder captar el primer valor y empezar la comparacion
	int i=0;
	for (i=0;i<24;i++){//for donde compara los promedios 
		//printf("lawe\n");
		//if (i=0)pr_ma=curso[i].prom;
		if (curso[i].prom>pr_ma)pr_ma=curso[i].prom;//si el valor actual es mayor que el da la variable, entonces la  variable tomara un nuevo valor(el promedio mayor que esta)
	}

	return pr_ma;//retorna el promedio mayor
}
//funcion para medir el promedio de un estudiante basado el procentaje que tiene cada evaluacion
void promedio(estudiante curso[], int i){
curso[i].prom=(curso[i].asig_1.proy1*0.20)+(curso[i].asig_1.proy2*0.20)+(curso[i].asig_1.proy3*0.30)+(curso[i].asig_1.cont1*0.05)+(curso[i].asig_1.cont2*0.05)+(curso[i].asig_1.cont3*0.05)+(curso[i].asig_1.cont4*0.05)+(curso[i].asig_1.cont5*0.05)+(curso[i].asig_1.cont6*0.05);
}
//funcion para poder digitar todas las notas de todos los alumnos, con sus promedios  y guardarlas en un arreglo
void registroCurso(estudiante curso[]){
	int i;
	for (i=0;i<24;i++){
		printf("Ingrese proyecto 1 del  estudiante %d \n",i);
		scanf("%f",&curso[i].asig_1.proy1);
		printf("Ingrese proyecto 2 del  estudiante %d \n",i);
		scanf("%f",&curso[i].asig_1.proy2);
		printf("Ingrese proyecto 3 del  estudiante %d \n",i);
		scanf("%f",&curso[i].asig_1.proy3);
		printf("Ingrese control 1 del  estudiante %d \n",i);
		scanf("%f",&curso[i].asig_1.cont1);
		printf("Ingrese control 2 del  estudiante %d \n",i);
		scanf("%f",&curso[i].asig_1.cont2);
		printf("Ingrese control 3 del  estudiante %d \n",i);
		scanf("%f",&curso[i].asig_1.cont3);
		printf("Ingrese control 4 del  estudiante %d \n",i);
		scanf("%f",&curso[i].asig_1.cont4);
		printf("Ingrese control 5 del  estudiante %d \n",i);
		scanf("%f",&curso[i].asig_1.cont5);
		printf("Ingrese control 6 del  estudiante %d \n",i);
		scanf("%f",&curso[i].asig_1.cont6);
		promedio(curso,i);
	}//debe registrar las calificaciones 
	//printf("TEST"); 
}
//funcion para generar dos archivos basados en los promedios  de los  estudiantes
void clasificarEstudiantes(char path[], estudiante curso[]){
	//si el alumno tienne un promedio mayor a 4.0 entonces entrara a este primer archivo 
	FILE *ap;
	if((ap=fopen("sobrevivientes","w"))==NULL){
		printf("\nerror al crear el archivo");
		exit(0); 
	}
	else{

		for (int i = 0; i<24; i++){//se recorre un for y si el promedio es mayor o igual a 4 este entrara a este archivo
		if(curso[i].prom>=4.0)fprintf(ap,"%s\t%s\t%s\t%.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].prom);
		}
	} 
	printf("\n **El registro de aprobados fue almacenado de manera correcta**\n \n ");
	fclose(ap);
	//si el  promedio del  estudiante es menor a 4.0 entonces este estara en el archivo caidos 
	FILE *rep;
	if((rep=fopen("caidos","w"))==NULL){
		printf("\nerror al crear el archivo");
		exit(0); 
	}
	else{
		for (int i = 0; i<24; i++){//se recorren todos los promedios y los que sean >4 entraran a este archivo
			if (curso[i].prom<4.0)fprintf(rep,"%s\t%s\t%s\t%.1f\n", curso[i].nombre,curso[i].apellidoP, curso[i].apellidoM, curso[i].prom);

		}		
	} 
	printf("\n **El registro de reprobados fue almacenado de manera correcta**\n \n ");
	fclose(rep);

 }
	//printf("TEST"); 


//funcion  que muestra el promedio maximo, el promedio minimo y la desviacion estandar
void metricasEstudiantes(estudiante curso[]){
	printf ("entro\n");
	float mayori=mayor(curso),men=menor(curso),des_est=desvStd(curso);
	printf("\nEl promedio mayor fue: %.2f\nEl promedio menor fue : %.2f\nLa desviacion estandar es: %.3f\n",mayori,men,des_est); 
}




//interfaz para el usuario pueda ver las opciones a elegir
void menu(estudiante curso[]){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
	estudiante curso[30]; 
	menu(curso); 
	return EXIT_SUCCESS; 
}